#ifndef GRAPH_CUH
#define GRAPH_CUH

#include <cuda.h>

template <typename T>    
__global__ void kernel(T *dev, size_t N) {
    if (threadIdx.x < N)
        dev[threadIdx.x] = threadIdx.x;
}    

#endif
