#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <string>

template <typename T>
class Graph {
    public:
        Graph() : Name("") {}

        Graph(const std::string &name) : Name(name) {}

        void initialize() {
            _data = new T[1024];
        }

        void run_on_gpu(size_t N);

    public:
        std::string Name;

    private:
        T *_data;
};

#endif
