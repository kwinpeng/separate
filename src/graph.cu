#include "graph.h"

#include "gpu/graph.cuh"

#include <cuda.h>

template <typename T>
void Graph<T>::run_on_gpu(size_t N) {
    T *dev;
    cudaMalloc(&dev, sizeof(T) * N);

    kernel<<<1, N>>>(dev, N);

    if (cudaDeviceSynchronize() != cudaSuccess) {
        std::cout << "CUDA error " << cudaGetErrorString(cudaGetLastError())
                  << std::endl;
    }
}

template class Graph<double>;
