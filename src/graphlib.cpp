#include <pybind11/pybind11.h>

#include "graph.h"

namespace py = pybind11;

PYBIND11_MODULE(graphlib, module) {
    py::class_<Graph<double>>(module, "Graph")
        .def(py::init<const std::string &>())
        .def("run_on_gpu", &Graph<double>::run_on_gpu);
}
